import Foundation

typealias APICallbackBlock = (Bool, Data?, NSError?) -> Void

class APIHelper: NSObject {
    
    class var shared: APIHelper {
        struct Static {
            static let instance: APIHelper = APIHelper()
        }
        
        return Static.instance
    }
    
    func categoriesList(completion: APICallbackBlock?) {
        guard let url = URL(string: APIConstants.APIServerUrl + APIConstants.APIList + APIConstants.APICategoriesList) else { return }
        
        defualtRequest(url, completion: completion)
    }
    
    func drinkCategory(_ category: String, completion: APICallbackBlock?) {
        guard let url = URL(string: APIConstants.APIServerUrl + APIConstants.APIFilter + APIConstants.APICategoryKey + (category.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "")) else { return }
        
        defualtRequest(url, completion: completion)
    }
    
    func defualtRequest(_ url: URL, completion: APICallbackBlock?) {
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            if let error = error as NSError? {
                completion?(false, nil, error)
            } else {
                completion?(true, data, nil)
            }
        }.resume()
    }
}
