protocol CategoryModelType: Codable {
    var strCategory: String? { get }
    var isShown: Bool { get set }
}

struct CategoryModel: CategoryModelType {
    var strCategory: String?
    var isShown: Bool = true
    
    enum CodingKeys: String, CodingKey {
        case strCategory = "strCategory"
        case isShown = "isShown"
    }
    
    init(from decoder: Decoder) throws {
        do {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            
            self.strCategory = try container.decodeIfPresent(String.self, forKey: .strCategory)
            self.isShown = true
        }
        catch {
            debugPrint(error)
            throw error
        }
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(self.strCategory, forKey: .strCategory)
        try container.encode(self.isShown, forKey: .isShown)
    }
}
