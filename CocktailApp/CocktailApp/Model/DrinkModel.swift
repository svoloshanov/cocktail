protocol DrinkModelType: Codable {
    var strDrink: String? { get }
    var idDrink: String? { get }
    var strDrinkThumb: String? { get }
}

struct DrinkModel: DrinkModelType {
    var strDrink: String?
    var idDrink: String?
    var strDrinkThumb: String?
    
    enum CodingKeys: String, CodingKey {
        case strDrink = "strDrink"
        case idDrink = "idDrink"
        case strDrinkThumb = "strDrinkThumb"
    }
    
    init(from decoder: Decoder) throws {
        do {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            
            self.strDrink = try container.decodeIfPresent(String.self, forKey: .strDrink)
            self.idDrink = try container.decodeIfPresent(String.self, forKey: .idDrink)
            self.strDrinkThumb = try container.decodeIfPresent(String.self, forKey: .strDrinkThumb)
        }
        catch {
            debugPrint(error)
            throw error
        }
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(self.strDrink, forKey: .strDrink)
        try container.encodeIfPresent(self.idDrink, forKey: .idDrink)
        try container.encodeIfPresent(self.strDrinkThumb, forKey: .strDrinkThumb)
    }
}
