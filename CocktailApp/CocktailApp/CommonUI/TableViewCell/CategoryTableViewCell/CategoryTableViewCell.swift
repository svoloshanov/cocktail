import UIKit

final class CategoryTableViewCell: UITableViewCell {
    
    @IBOutlet weak var categoryLabel: UILabel!
    
    func setupLabel(_ text: String?) {
        categoryLabel.font = FontScheme.robotoRegular16
        categoryLabel.text = text
    }
    
    func setupChakmark() {
        
    }
}
