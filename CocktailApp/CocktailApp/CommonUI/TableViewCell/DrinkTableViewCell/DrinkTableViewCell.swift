import UIKit
import Kingfisher

final class DrinkTableViewCell: UITableViewCell {

    @IBOutlet weak var drinkImageView: UIImageView!
    @IBOutlet weak var drinkDescriptionLabel: UILabel!
    
    func setupViews(_ item: DrinkModel) {
        setupLabel(item.strDrink)
        loadImage(item.strDrinkThumb)
    }
    
    func setupLabel(_ text: String?) {
        drinkDescriptionLabel.font = FontScheme.robotoRegular16
        drinkDescriptionLabel.text = text
    }
    
    func loadImage(_ imageURL: String?) {
        guard let imageURL = imageURL, let url = URL(string: imageURL) else { return }
        drinkImageView.kf.setImage(with: url)
    }
}
