import UIKit

struct APIConstants {
    static let APIVirsion = "api/json/v1/1/"
    static let APIServerUrl = "https://www.thecocktaildb.com/" + APIVirsion
    
    static let APIList = "list.php"
    static let APIFilter = "filter.php"
    static let APICategoryKey = "?c="
    static let APICategoriesList = APICategoryKey + "list"
    static let APIDrinksKey = "drinks"
}

struct StringValue {
    static let drinksTitle = "Drinks"
    static let filtersTitle = "Filters"
}

struct ImageName {
    static let backButton = "Back"
    static let filtersButton = "Filter"
}

struct FontScheme {
    static let robotoRegular14: UIFont = UIFont(name: "Roboto-Regular", size: 14) ?? .systemFont(ofSize: 14, weight: .regular)
    static let robotoRegular16: UIFont = UIFont(name: "Roboto-Regular", size: 16) ?? .systemFont(ofSize: 16, weight: .regular)
    static let robotoMedium24: UIFont = UIFont(name: "Roboto-Medium", size: 24) ?? .systemFont(ofSize: 24, weight: .medium)
    static let robotoBold16: UIFont = UIFont(name: "Roboto-Bold", size: 16) ?? .systemFont(ofSize: 16, weight: .bold)
}
