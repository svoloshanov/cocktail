import UIKit

final class FilterViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    weak var delegate: FilterViewControllerDelegate?
    
    var filterList: [CategoryModel] = []
    let bottomFreeSpace: CGFloat = 160.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
    func setupViews() {
        setupTableView()
        setupNavigationBar()
    }
    
    func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.contentInset.bottom = bottomFreeSpace
        
        tableView.register(CategoryTableViewCell.self)
    }
    
    func setupNavigationBar() {
        DispatchQueue.main.async {
            guard let topVC = self.navigationController?.topViewController else { return }
            
            topVC.title = StringValue.filtersTitle
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: FontScheme.robotoMedium24]
            
            self.setupBackButton()
        }
    }
    
    func setupBackButton() {
        guard let navBar =  self.navigationController?.navigationBar else { return }
        navBar.topItem?.backBarButtonItem?.action = #selector(backTapped)
    }
    
    @objc func backTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    //MARK:- IBAction
    @IBAction func applyFilters(_ sender: Any) {
        delegate?.updateFilter(filterList)
        backTapped()
    }
}
//MARK:- UITableViewDelegate, UITableViewDataSource
extension FilterViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataManager.shared.categories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(CategoryTableViewCell.self, for: indexPath)
        let item = DataManager.shared.categories[indexPath.row]
        cell.setupLabel(item.strCategory)
        cell.accessoryType = filterList[indexPath.row].isShown ? .checkmark : .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if let cell = tableView.cellForRow(at: indexPath) {
            filterList[indexPath.row].isShown = !filterList[indexPath.row].isShown
            cell.accessoryType = cell.accessoryType == .none ? .checkmark : .none
        }
    }
}
