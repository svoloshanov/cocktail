import UIKit

protocol FilterViewControllerDelegate: class {
    func updateFilter(_ list: [CategoryModel])
}

final class DrinksViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var categoryIndex: Int = 0
    
    var list: [SectionModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadCategories()
        setupViews()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationItem.title = " "
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = StringValue.drinksTitle
    }
    
    func setupViews() {
        setupTableView()
        setupNavigationBar()
    }
    
    func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.register(DrinkTableViewCell.self)
    }
    
    func setupNavigationBar() {
        DispatchQueue.main.async {
            guard let topVC = self.navigationController?.topViewController else { return }
            
            topVC.title = StringValue.drinksTitle
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: FontScheme.robotoMedium24]
            
            topVC.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: ImageName.filtersButton), style: .plain, target: self, action: #selector(self.filter))
            
        }
    }
    
    func loadCategories() {
        APIHelper.shared.categoriesList { (status, data, error) in
            DispatchQueue.main.async {
                if status {
                    if let data = data, let list = Utils.fromDataToTypeList(CategoryModel.self, data: data) as? [CategoryModel] {
                        DataManager.shared.categories = list
                        self.loadDrinks()
                    }
                } else if let error = error {
                    print(error.localizedDescription)
                }
            }
        }
    }
    
    func loadDrinks() {
        let categories = DataManager.shared.categories.filter({ $0.isShown })
        if let category = categories[categoryIndex].strCategory, categories.count > categoryIndex {
            APIHelper.shared.drinkCategory(category) { (status, data, error) in
                DispatchQueue.main.async {
                    if status {
                        if let data = data, let list = Utils.fromDataToTypeList(DrinkModel.self, data: data) as? [DrinkModel] {
                            let section = SectionModel(strCategory: DataManager.shared.categories.filter({ $0.isShown })[self.categoryIndex].strCategory, drinkList: list)
                            self.list.append(section)
                            self.categoryIndex+=1
                            self.tableView.reloadData()
                        }
                    } else if let error = error {
                        print(error.localizedDescription)
                    }
                }
            }
        } else {
            print("load all categories")
        }
    }
    
    @objc func filter() {
        let vc = FilterViewController.instance()
        vc.filterList = DataManager.shared.categories
        vc.delegate = self
        
        self.navigationController?.push(vc, animated: true)
    }
}
//MARK:- UITableViewDelegate, UITableViewDataSource
extension DrinksViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        view.tintColor = .white
        if let header = view as? UITableViewHeaderFooterView {
            header.textLabel?.textColor = .black
            header.textLabel?.font = FontScheme.robotoRegular14
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return list[section].strCategory
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list[section].drinkList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(DrinkTableViewCell.self, for: indexPath)
        cell.setupViews(list[indexPath.section].drinkList[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.section == list.count - 1 && indexPath.row == (list.last?.drinkList.count ?? 0) - 1 {
            self.loadDrinks()
        }
    }
}
//MARK:- FilterViewControllerDelegate
extension DrinksViewController: FilterViewControllerDelegate {
    func updateFilter(_ list: [CategoryModel]) {
        self.list = []
        DataManager.shared.categories = list
        categoryIndex = 0
        loadDrinks()
    }
}
