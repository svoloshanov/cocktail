import UIKit

extension UITableViewCell: NibLoadable {}
extension UITableViewHeaderFooterView: NibLoadable {}

protocol NibLoadable {
    static var nib: UINib { get }
    static var identifier: String { get }
}

extension NibLoadable {
    static var nib: UINib {
        return UINib(nibName: identifier, bundle: Bundle.main)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
}

extension UINib {
    convenience init<T>(_: T.Type) {
        self.init(nibName: String(describing: T.self), bundle: Bundle.main)
    }
}

extension UITableView {
    func register<T: UITableViewCell>(_:T.Type) {
        self.register(T.nib, forCellReuseIdentifier: T.identifier)
    }
    
    func register<T: UITableViewHeaderFooterView>(_:T.Type) {
        self.register(T.nib, forHeaderFooterViewReuseIdentifier: T.identifier)
    }
    
    func dequeue<T: UITableViewCell>(_: T.Type) -> T {
        return self.dequeueReusableCell(withIdentifier: T.identifier) as! T
    }
    
    func dequeue<T: UITableViewCell>(_: T.Type, for indexPath: IndexPath) -> T {
        return self.dequeueReusableCell(withIdentifier: T.identifier, for: indexPath) as! T
    }
}
