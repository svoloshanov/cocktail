import UIKit

extension UIStoryboard {
    class func instantiate<T: UIViewController>(_: T.Type) -> T {
        return UIStoryboard(name: T.identifier, bundle: nil).instantiateInitialViewController() as! T
    }
}
