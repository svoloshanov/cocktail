import UIKit

protocol Identifiable {
    static var identifier: String { get }
}

extension UIViewController: Identifiable {}

extension Identifiable {
    static var identifier: String {
        return String(describing: self)
    }
}
extension UINavigationController {
    func push(_ viewController: UIViewController, animated: Bool = true) {
        pushViewController(viewController, animated: true)
    }
}

extension UIStoryboard {
    enum Storyboard: String {
        case main = "Main"
    }
    
    convenience init(storyboard: Storyboard) {
        self.init(name: storyboard.rawValue, bundle: nil)
    }
    
    func instantiateViewController<T: UIViewController>(_ type: T.Type) -> T {
        let id = NSStringFromClass(T.self).components(separatedBy: ".").last!
        return self.instantiateViewController(withIdentifier: id) as! T
    }
}

extension UIViewController {
    class func instance() -> Self {
        let storyboard = UIStoryboard(storyboard: .main)
        let viewController = storyboard.instantiateViewController(self)
        return viewController
    }
}
