import UIKit

class Utils {
    class func fromDataToTypeList<T: Codable>(_:T.Type, data: Data) -> [Any] {
        do {
            let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            
            if let list = json?[APIConstants.APIDrinksKey], let data = try? JSONSerialization.data(withJSONObject: list, options: []), let array = try? JSONDecoder().decode([T].self, from: data) {
                return array
            }
        } catch { }
        return []
    }
    
    func checkArray() {
        
    }
}
